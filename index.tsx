import { component, React, Style, Children } from 'local/react/component'

export const defaultPictureStyle: Style = {
  backgroundSize: 'cover',
  display: 'inline-block',
  minHeight: '1em',
  minWidth: '1em',
  position: 'relative'
}

export const defaultPictureCaptionStyle: Style = {
  position: 'absolute',
  bottom: '0',
  left: '0',
  right: '0',
  padding: '1em',
  background: 'rgba(0, 0, 0, 0.5)',
  color: '#fff',
  overflow: 'hidden',
  textOverflow: 'ellipsis'
}

export const Picture = component.children
  .props<{
    className?: string
    children?: Children
    src: string
    style?: Style
    captionStyle?: Style
  }>({
    style: defaultPictureStyle,
    captionStyle: defaultPictureCaptionStyle
  })
  .render(({ className, children, src, style, captionStyle }) => {
    const combinedStyle = {
      ...style,
      backgroundImage: `url("${src}")`
    }
    return (
      <div style={combinedStyle} className={className}>
        {children ? <div style={captionStyle}>{children}</div> : undefined}
      </div>
    )
  })
